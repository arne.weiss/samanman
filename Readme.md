# Selenium WebDriver Script for Visametric

This project provides a Python script to automate the process of navigating and filling out a form on the Visametric website. It uses the Selenium WebDriver library and the geckodriver to control a Firefox browser. This README provides detailed instructions for setting up and running the script on Linux and macOS using Nix.

## Table of Contents

1. [Requirements](#requirements)
2. [Installation](#installation)
   - [Linux](#linux)
   - [macOS](#macos)
3. [Running the script](#running-the-script)
4. [Extending the script](#extending-the-script)
5. [Troubleshooting](#troubleshooting)

## Requirements

- Python 3.6 or later
- Firefox browser
- Nix package manager
- geckodriver (Firefox WebDriver for Selenium)

## Installation

### Linux

1. Install the Nix package manager if you haven't already by running:

   <code>curl -L https://nixos.org/nix/install | sh</code>

2. Add the following lines to your <code>~/.bashrc</code> or <code>~/.zshrc</code> file:

   <code>source ~/.nix-profile/etc/profile.d/nix.sh</code>

   Restart your terminal or run <code>source ~/.bashrc</code> or <code>source ~/.zshrc</code> to apply the changes.

3. Install geckodriver using Nix:

   <code>nix-env -iA nixpkgs.geckodriver</code>

### macOS

1. Install the Nix package manager if you haven't already by running:

   <code>curl -L https://nixos.org/nix/install | sh</code>

2. Add the following lines to your <code>~/.bashrc</code> or <code>~/.zshrc</code> file:

   <code>source ~/.nix-profile/etc/profile.d/nix.sh</code>

   Restart your terminal or run <code>source ~/.bashrc</code> or <code>source ~/.zshrc</code> to apply the changes.

3. Install geckodriver using Nix:

   <code>nix-env -iA nixpkgs.geckodriver</code>

## Running the script

1. Make sure you have the script file <code>test.py</code> in your working directory.

2. In the terminal, navigate to the directory containing the script and run:

   <code>python test.py</code>

3. The script will open a Firefox browser window, navigate to the target website, and interact with the specified elements. The user will need to manually click the submit button when prompted.

4. After the submit button is clicked, the script will continue to interact with the elements on the new page.

## Extending the script

To extend the script, you can add more interactions with the web page elements using the Selenium WebDriver API. Make sure to use the provided helper functions (<code>wait_and_click</code>, <code>option_present_in_select</code>, and <code>select_option</code>) to handle waiting for elements to appear and interacting with them.

## Troubleshooting

- If you encounter issues with the geckodriver path, make sure that the path is correctly set in the script, and that the geckodriver is installed in the specified location.

- If you encounter issues with element interactions, make sure that the elements are present and visible on the web page, and that the script is using the correct element identifiers (e.g., IDs or class names
