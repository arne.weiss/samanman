with import <nixpkgs-unstable> {};

#     geckodriver
#     python38
#     python38Packages.selenium

with import <nixpkgs> {};

let
  python = let 
    packageOverrides = prev: final: {
      selenium = final.selenium.overridePythonAttrs (old: {
        src = fetchFromGitHub {
          owner = "SeleniumHQ";
          repo = "selenium";
          rev = "refs/tags/selenium-4.8.0";
          hash = "sha256-YTi6SNtTWuEPlQ3PTeis9osvtnWmZ7SRQbne9fefdco=";
        };
        postInstall = ''
          install -Dm 755 ../rb/lib/selenium/webdriver/atoms/getAttribute.js $out/${python3Packages.python.sitePackages}/selenium/webdriver/remote/getAttribute.js
          install -Dm 755 ../rb/lib/selenium/webdriver/atoms/isDisplayed.js $out/${python3Packages.python.sitePackages}/selenium/webdriver/remote/isDisplayed.js
        '';
      });
    }; in python3.override { inherit packageOverrides; };
in mkShell {
  buildInputs = [
    (python.withPackages (
      ps: with ps; [
	pandas
	requests
        selenium
      ]
    ))
    geckodriver
  ];
}

