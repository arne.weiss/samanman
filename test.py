from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import os

def main():
    # Define the target URL
    url = "https://ir-appointment.visametric.com/en"

    # Get the geckodriver path and create a Firefox webdriver
    geckodriver_path = os.environ.get('GECKODRIVER_PATH', '/nix/store/2nrjhmvqiirig1w3zhxs9mwhg1z9yprf-geckodriver-0.32.2/bin/geckodriver')
    service = Service(executable_path=geckodriver_path)
    driver = webdriver.Firefox(service=service)

    # Open the target URL
    driver.get(url)

    try:
        # Click the element with id "legalizationBtn"
        wait_and_click(driver, By.ID, "legalizationBtn")

        # Click the element with id "result0"
        wait_and_click(driver, By.ID, "result0")

        # Click the element with id "result1"
        wait_and_click(driver, By.ID, "result1")

        # Wait for the user to manually click the submit button
        wait = WebDriverWait(driver, 120)  # Wait up to 120 seconds for user action
        wait.until(lambda driver: driver.current_url == "https://ir-appointment.visametric.com/en/appointment-form")

        # Wait for the "ILAM" option to be present in the "city" select element and select it
        wait.until(option_present_in_select(By.ID, "city", "ILAM"))
        select_option(driver, By.ID, "city", "ILAM")

        # Wait for the "TEHRAN" option to be present in the "office" select element and select it
        wait.until(option_present_in_select(By.ID, "office", "TEHRAN"))
        select_option(driver, By.ID, "office", "TEHRAN")

    except Exception as e:
        print("An error occurred:", e)

    finally:
        time.sleep(60)  # Wait for 60 seconds to see the result
        driver.quit()  # Close the browser

def wait_and_click(driver, by, value):
    # Wait for an element to be clickable and then click on it
    wait = WebDriverWait(driver, 10)  # Wait up to 10 seconds
    element = wait.until(EC.element_to_be_clickable((by, value)))
    element.click()

def option_present_in_select(by, value, option_text):
    # Check if an option is present in a select element
    def _option_present(driver):
        select_element = driver.find_element(by, value)
        for option in select_element.find_elements(By.TAG_NAME, "option"):
            if option.text == option_text:
                return True
        return False

    return _option_present

def select_option(driver, by, value, option_text):
    # Select an option in a select element by its text
    select_element = driver.find_element(by, value)
    for option in select_element.find_elements(By.TAG_NAME, "option"):
        if option.text == option_text:
            option.click()
            break

if __name__ == "__main__":
    main()
